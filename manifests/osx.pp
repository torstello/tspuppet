$username     =  'ts'
$groupname    =  'staff'
$home         =  "/Users/$username"
$ruby_version = '2.1.2'
$source_dir   =  "$home/SOURCE"

$git_user   = 'torstello'

Exec { path => [ "${home}/bin", "${home}/.rbenv/shims", '/usr/sbin', '/sbin', '/usr/local/bin', '/usr/bin', '/bin' ] }

include rbenv
include gems::update
include git

package { ['findutils', 'tag', 'tmux', 'tree', 'wget', 'wput', 'zsh' ]:
  ensure  => installed,
  provider => brew
}
exec {
  'gsed':
    command => "mv /usr/bin/sed /usr/bin/sed.orig",
    creates => '/usr/bin/sed.orig';
  'gcal':
    command => "mv /usr/bin/cal /usr/bin/cal.orig",
    creates => '/usr/bin/cal.orig';
  'zsh_files':
    command => "/bin/ln -sf $home/tsdotfiles/.zsh/*.zsh $home/.zsh/";
    # creates => "$HOME/.zsh/setopt.zsh";
}

package {
  'gcal':
    ensure   => installed,
    require  => Exec  ['gcal'],
    provider =>  brew;
  'gnu-sed':
    ensure   => installed,
    require  => Exec['gsed'],
    provider =>  brew;
}

file {
  $source_dir:
    ensure  => 'directory',
    owner   => $username,
    group   => $groupname;
  "$home/.ssh":
    ensure  => 'directory',
    mode    => '0700',
    owner   => $username,
    group   => $groupname;

  "$home/.zshrc":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.zshrc";
  "$home/.vimrc":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.vimrc";
  "$home/.irbrc":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.irbrc";
  "$home/.tmux.conf":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.tmux.conf";
  "$home/.ssh/config":
    ensure  => 'link',
    target  => "$home/tsdotfiles/ssh-config",
    require => File["$home/.ssh"]
}

rbenv::install { 'ruby_2.0': version => $ruby_version, before => Class['gems'] }

git::clone { 'tmt':
  path    => "$source_dir/tmt",
  source  => "https://$git_user:$git_token@github.com/torstello/tmt.git",
  require => File[ $source_dir ]
}
git::clone { 'tsdotfiles':
  path    => "$home/tsdotfiles",
  source  => "https://github.com/$git_user/tsdotfiles.git",
  require => File[ $source_dir ]
}
git::clone { 'tspuppet':
  path    => "$home/tspuppet",
  source  => "https://github.com/$git_user/tspuppet.git",
  require => File[ $source_dir ]
}
git::clone { 'tsvagrant':
  path    => "$home/tsvagrant",
  source  => "https://github.com/$git_user/tsvagrant.git",
  require => File[ $source_dir ]
}


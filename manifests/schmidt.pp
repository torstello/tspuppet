$username   =  'schmidt'
$groupname  =  'itcologne'

case $domain {
  "noc.fiducia.de": { $home = "/work/$username" }
  "aws":            { $home = "/work/$username" }
  default:          { $home =  "/home/$username" }
}

$source_dir =  "$home/SOURCE"
$git_user   = 'torstello'

exec {
  'zsh_files':
    command => "/bin/ln -sf $home/tsdotfiles/.zsh/*.zsh $home/.zsh/";
    # creates => "$HOME/.zsh/setopt.zsh";
}

file {
  [ "$home/bin", $source_dir, "$home/.zsh", "$home/.zsh/func" ]:
    ensure  => 'directory',
    owner   => $username,
    group   => $groupname;
  "$home/.ssh":
    ensure  => 'directory',
    mode    => '0700',
    owner   => $username,
    group   => $groupname;
  "$home/.zshrc":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.zshrc";
  "$home/.zshenv":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.zshenv";
  "$home/.zsh/func/_tmt":
    ensure  => 'link',
    target  => "$source_dir/tmt/_tmt";
  "$home/.vimrc":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.vimrc";
  "$home/.irbrc":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.irbrc";
  "$home/.rpmmacros":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.rpmmacros";
  "$home/.tmux.conf":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.tmux.conf";
  "$home/.ssh/config":
    ensure  => 'link',
    target  => "$home/tsdotfiles/ssh-config",
    require => File["$home/.ssh"]
}

Exec { path => [ '/usr/sbin', '/sbin', '/usr/local/bin', '/usr/bin', '/bin' ] }

package { [
          'make',
          'patch',
          'readline',
          'tree',
          'vim-enhanced',
          'zsh',
          ]:
            ensure  => installed
}

include git
git::clone { 'tmt':
  path    => "$source_dir/tmt",
  source  => "https://$git_user:$git_token@github.com/torstello/tmt.git",
  require => File[ $source_dir ]
}

git::clone { 'tsdotfiles':
  path    => "$home/tsdotfiles",
  source  => "https://github.com/$git_user/tsdotfiles.git"
}

$username   =  'ts'
$groupname  =  'admin'
$home       =  "/home/$username"
$source_dir =  "$home/SOURCE"

$git_user   = 'torstello'


group {
  $groupname:
    ensure => present,
    gid    => 111111,
    before => User[ $username ]
}
user {
  $username:
    ensure     => present,
    uid        => 111111,
    gid        => 111111,
    home       => $home,
    managehome => true,
    shell      => '/bin/zsh',
    membership => 'minimum',
    before     => Ssh_authorized_key[ $username_key ]
}
ssh_authorized_key {
  $username_key:
    ensure  => present,
    key     => 'AAAAB3NzaC1yc2EAAAABJQAAAIEAm4p1VVAmlJq9fWlaGLa14zFEYkCrL2Ghfh5nQvL1e7FOXk9VP0APb8xHmkSS4nKg763zJg2jGtA4Aie2qft+96VEPwx1LLbu2FKQCg8HO4IYaiNJ8JojHYapsWvGkmdSCUOyEUeSio+iSdgpq9R0qr8OGSugLDrmIhT5EAVUlv8=',
    type    => 'ssh-rsa',
    user    => $username
}

file {
  $source_dir:
    ensure  => 'directory',
    owner   => $username,
    group   => $groupname;
  "$home/.ssh":
    ensure  => 'directory',
    mode    => '0700',
    owner   => $username,
    group   => $groupname;

  "$home/.zshrc":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.zshrc";
  "$home/.zshenv":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.zshenv";
  "$home/.vimrc":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.vimrc";
  "$home/.irbrc":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.irbrc";
  "$home/.tmux.conf":
    ensure  => 'link',
    target  => "$home/tsdotfiles/.tmux.conf";
  "$home/.ssh/config":
    ensure  => 'link',
    target  => "$home/tsdotfiles/ssh-config",
    require => File["$home/.ssh"]
}

Exec { path => [ '/usr/sbin', '/sbin', '/usr/local/bin', '/usr/bin', '/bin' ] }

$cmd = $operatingsystem ? {
  /Debian/          => 'apt-get update',
  /(RedHat|CentOS)/ => 'yum -y update'
}

exec { 'os-update':
          command => $cmd,
          unless  => "test -e ${source_dir}"
}

operatingsystem_packages { [
          'autoconf',
          'automake',
          'bison',
          'bzip2',
          'curl',
          'firefox',
          'gcc-c++',
          'libffi-devel',
          'libtool',
          'libyaml-devel',
          'make',
          'openssl-devel',
          'patch',
          'readline',
          'readline-devel',
          'rubygems',
          'tmux',
          'tree',
          'vim-enhanced',
          'zlib',
          'zlib-devel',
          'zsh',
          ]:
}
deb_packages { [
          'automake',
          'bison',
          'bzip2',
          'curl',
          'libtool',
          'make',
          'patch',
          'readline-common',
          'rubygems',
          'tmux',
          'tree',
          'vim',
          'zsh',
          ]:
            ensure  => installed,
            require => Exec['os-update']
}

# for this to work the /home/vagrant directory needs 755 permissions...
include git
git::clone { 'tmt':
  path    => "$source_dir/tmt",
  source  => "https://$git_user:$git_token@github.com/torstello/tmt.git",
  require => File[ $source_dir ]
}

git::clone { 'tsdotfiles':
  path    => "$home/tsdotfiles",
  source  => "https://github.com/$git_user/tsdotfiles.git",
  require => File[ $source_dir ]
}

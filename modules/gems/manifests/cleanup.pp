class gems::cleanup {
    exec { 'gem cleanup':
     command => 'gem cleanup',
     require => Class['gem::update']
    }
}

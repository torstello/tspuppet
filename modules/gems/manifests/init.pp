class gems {

  $gems = [
    'aruba',
    'awesome_print',
    'bundler',
    'cheat',
    'cheatset',
    'cucumber',
    'facter',
    'gem-man',
    'geminabox',
    'gherkin',
    #'git',  # not possible b/o of name conflict
    'github_api',
    'gli',
    'hiera',
    'interactive_editor',
    'methadone',
    'nokogiri',
    'puppet',
    'puppet-lint',
    'rack',
    'rake',
    'ramaze',
    #'rdoc',
    'rspec',
    'rubygems-tasks',
    'rubygems-update',
    'rye',
    'sinatra',
    'slim',
    'tmuxinator',
    'unicorn',
    'yard'
  ]
  package { $gems:
    ensure => installed,
    provider => 'gem',
    require => Package['rbenv']
  }
}


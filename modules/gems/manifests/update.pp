class gems::update {
  include 'gems'
    exec { 'gem update':
     command => 'gem cleanup',
     require => Class['gems']
    }
}

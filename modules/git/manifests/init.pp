class git {

  $provider = $operatingsystem ? {
    /(RedHat|CentOS)/  =>  'yum',
    /Debian/           =>  'apt',
    /Darwin/           =>  'brew'
  }

  package { 'git': ensure => latest, provider => $provider }
}

define git::clone( $path, $source) {
  exec { "git_clone_${name}":
          command  => "git clone ${source} ${path}",
          # do nothing, if this exists:
          creates   => "${path}/.git",
          #user      => $username,
          require   => Package[ 'git'],
          timeout   => 0
  }
}

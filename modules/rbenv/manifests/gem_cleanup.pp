class rbenv::gem_cleanup {
    exec { 'gem cleanup':
     command => 'gem cleanup',
     require => Class['rbenv']
    }
}

